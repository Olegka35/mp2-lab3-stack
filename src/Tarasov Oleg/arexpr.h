#ifndef  __ARIFM_EXPR__
#define __ARIFM_EXPR__
#include <iostream>
#include <string>
#include <stack>
#include "../include/tdatstack.h"
using namespace std;


int CheckExpression(string str) {
	TStack openBraces(10);
	int braceNum = 1, errors = 0;
	cout << "Expression: " << str << endl;
	cout << "Braces print: " << endl;
	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		if (*iter == '(') {
			openBraces.Put(braceNum);
			braceNum++;
		}
		else if (*iter == ')') {
			if (openBraces.IsEmpty()) {
				cout << "0 - " << braceNum++ << endl;
				errors++;
			}
			else {
				cout << openBraces.Pop() << " - " << braceNum++ << endl;
			}
		}
	}
	while (!openBraces.IsEmpty()) {
		cout << "0 - " << openBraces.Pop() << endl;
	}
	cout << "Errors: " << errors << endl << endl;
	return errors;
}

int GetPriority(char c) {
	switch (c) {
		case '(':
			return 0;
			break;
		case ')':
			return 1;
			break;
		case '+':
			return 2;
			break;
		case '-':
			return 2;
			break;
		case '*':
			return 3;
			break;
		case '/':
			return 3;
			break;
	}
	return -1;
}

string DeleteSpaces(string str) {
	for (string::iterator it = str.begin(); it<str.end(); ++it) {
		if (*it == ' ') str.erase(it);
	}
	return str;
}

string ConvertToPostfixForm(string str) { // ��������������� ��������� � ����������� �����
	TStack operations(10);
	string temp;
	bool apStarted = false;
	for (string::iterator it = str.begin(); it<str.end(); ++it) {
		int priority = GetPriority(*it);
		if (priority == -1) { // �������
			temp += *it;
			if (!apStarted) apStarted = true;
		}
		else {
			static int oldOp = -1;
			if (apStarted == true) {
				apStarted = false;
				temp += " ";
			}
			if (priority == 1) { // ��������� ����������� ������
				while (GetPriority(operations.Get()) != 0) {
					temp += operations.Pop();
				}
				operations.Pop();
			}
			else if (priority == 0 || priority > oldOp || operations.IsEmpty()) {
				oldOp = *it;
				operations.Put(oldOp);
			}
			else {
				while (GetPriority(operations.Get()) >= priority) {
					temp += operations.Pop();
				}
				temp += " ";
				oldOp = *it;
				operations.Put(oldOp);
			}
		}
	}
	while (!operations.IsEmpty()) {
		temp += operations.Pop();
	}
	return temp;
}

double ComputeExpression(string str) {
	string temp;
	stack<double> operands;
	for (string::iterator it = str.begin(); it<str.end(); ++it) {
		int priority = GetPriority(*it);
		if (priority == -1 && *it != ' ') { // �������
			temp += *it;
		}
		else {
			if (temp.length()) {
				operands.push(atof(temp.c_str()));
				temp.clear();
			}
			if (priority >= 2) { // ��������
				double val[2], result = 0;
				for (int i = 0; i < 2; i++) {
					val[i] = operands.top();
					operands.pop();
				}
				switch (*it) {
					case '+':
						result  = val[0] + val[1];
						break;
					case '-':
						result = val[1] - val[0];
						break;
					case '*':
						result = val[0] * val[1];
						break;
					case '/':
						result = val[1] / val[0];
						break;
				}
				operands.push(result);
			}
		}
	}
	return operands.top();
}

double Compute(string str) {
	str = DeleteSpaces(str);
	if (CheckExpression(str) == 0) {
		return ComputeExpression(ConvertToPostfixForm(str));
	}
	return 0;
}

#endif // ! __ARIFM_EXPR__
