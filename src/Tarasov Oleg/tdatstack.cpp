#include "../include/tdatstack.h"
#include <iostream>
using namespace std;

void TStack::Put(const TData &Val) {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsFull()) {
		SetMem(pMem, MemSize + 20);
		pMem[++top] = Val;
		DataCount++;
	}
	else {
		pMem[++top] = Val;
		DataCount++;
	}
}

TData TStack::Get() {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		return pMem[top];
	}
	return -1;
}

TData TStack::Pop() {
	if (!IsValid()) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;
}

int TStack::IsValid() {
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) return 0;
	return 1;
}

void TStack::Print() {
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}