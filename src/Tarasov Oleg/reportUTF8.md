﻿# Методы программирования 2: Стек

## Цели и задачи

Лабораторная работа направлена на практическое освоение динамической структуры данных Стек. В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.
При вычислении произвольных арифметических выражений возникают две основные задачи: проверка корректности введённого выражения и выполнение операций в порядке, определяемом их приоритетами и расстановкой скобок. Был рассмотрен алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. Рассмотренные в ходе выполнения данной лабораторной работы алгоритмы являются начальным введением в область машинных вычислений.

### Поставленные задачи
В рамках лабораторной работы была поставлена задача разработки двух видов стеков:
- прстейшего, основанного на статическом массиве (класс TSimpleStack);
- более сложного, основанного на использовании динамической структуры (класс TStack).
С помощью разработанных стеков было необходимо написать приложение, вычисляющее арифметическое выражение, заданное в виде строки и вводимое пользователем с клавиатуры. Сложность выражения ограничена только длиной строки.


### Дано
- Интерфейсы классов *TDataCom* и *TDataRoot* (h-файлы)
- Тестовый пример использования класса *TStack*


### План работы
1. Разработка класса **TSimpleStack** на основе массива фиксированной длины.
2. Реализация методов класса **TDataRoot** согласно заданному интерфейсу.
3. Разработка класса **TStack**, являющегося производным классом от *TDataRoot*.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.


### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **Microsoft Visual Studio 2015 Community Edition**.



## Выполнение работы

### 1. TSimpleStack.h

    #ifndef __SIMPLESTACK_H__
    #define __SIMPLESTACK_H__
    
    #define MemSize   25  // размер стека по умолчанию
    
    template <class TypeData = int, int N = MemSize>
    class TSimpleStack
    {
    private:
    	int Top;
    	TypeData Data[N];
    public:
    	TSimpleStack();
    	void Push(const TypeData &Val);
    	TypeData Pop();
    	bool IsEmpty() const { return Top == -1; }
    	bool IsFull() const { return Top == N; }
    	void Print();
    };
    
    template <class TypeData, int N>
    TSimpleStack <TypeData, N> ::TSimpleStack() : Top(-1)
    {
    }
    
    template <class TypeData, int N>
    void TSimpleStack <TypeData, N> ::Push(const TypeData &Val)
    {
    	if (IsFull()) throw "StackOverflow";
    	else
    	{
    		Data[++Top] = Val;
    	}
    }
    
    template <class TypeData, int N>
    TypeData TSimpleStack <TypeData, N> ::Pop()
    {
    	TypeData result = -1;
    	if (IsEmpty()) throw "StackIsEmpty";
    	else
    	{
    		result = Data[Top--];
    	}
    	return result;
    }
    
    template <class TypeData, int N>
    void TSimpleStack <TypeData, N> ::Print()
    {
    	for (int i = 0; i <= Top; i++)
    		cout << Data[i] << ' ';
    	cout << endl;
    }
    
    #endif
	
**Используемые методы:**

* *void Push(const TypeData &Val);* - Положить данные в стек
* *TypeData Pop();* - Взять верхний элемент стека
* *bool IsEmpty();* - Проверка пустоты стека
* *bool IsFull();* - Проверка заполненности стека
* *void Print();* - Распечатать стек


### 2. TDataRoot.cpp

Реализация методов класса *TDataRoot*: 

	#include "tdataroot.h"

	TDataRoot::TDataRoot(int Size=0) : TDataCom()
	{
		DataCount = 0;
		MemSize = Size;
		if (Size == 0) {
			MemType = MEM_RENTER;
			pMem = nullptr;
		}
		else {
			MemType = MEM_HOLDER;
			pMem = new TElem[MemSize];
		}
	}

	TDataRoot::~TDataRoot() {
		if (MemType == MEM_HOLDER) delete[] pMem;
		pMem = nullptr;
	}

	void TDataRoot::SetMem(TElem *p, int Size) {
		int minSize = (MemSize < Size) ? MemSize : Size;
		MemSize = Size;
		pMem = new TElem[Size];
		for (int i = 0; i < minSize; i++) {
			pMem[i] = p[i];
		}
		if(DataCount > MemSize) // если размер уменьшается
			DataCount = MemSize;
		delete[] p;
	}


	bool TDataRoot::IsEmpty(void) const {
		return DataCount == 0;
	}

	bool TDataRoot::IsFull(void) const {
		return DataCount == MemSize;
	}
	
**Описание методов:**
- *void TDataRoot::SetMem(void *p, int Size)* - Установить размер стека.
- *bool TDataRoot::IsEmpty(void)* - Проверка пустоты стека.
- *TDataRoot::IsFull(void)* - Проверка заполненности стека.


### 3. Класс **TStack**

**tdatstack.h**

	#ifndef __T_DAT_STACK__
	#define __T_DAT_STACK__

	#include "tdataroot.h"

	class TStack : public TDataRoot
	{
	private:
		int top;

	public:
		TStack(int Size = DefMemSize) : TDataRoot(Size), top(-1) { };
		virtual void Put(const TData &Val);
		virtual TData Pop();
		virtual TData Get();

		virtual int IsValid();
		virtual void Print();
	};

	#endif
	
**tdatstack.cpp**

	#include "tdatstack.h"
	#include <iostream>
	using namespace std;

	void TStack::Put(const TData &Val) {
		if (!IsValid()) SetRetCode(DataNoMem);
		else if (IsFull()) {
			SetMem(pMem, MemSize + 20);
		}
		else {
			pMem[++top] = Val;
			DataCount++;
		}
	}

	TData TStack::Get() {
		if (!IsValid()) SetRetCode(DataNoMem);
		else if (IsEmpty()) SetRetCode(DataEmpty);
		else {
			return pMem[top];
		}
		return -1;
	}

	TData TStack::Pop() {
		if (!IsValid()) SetRetCode(DataNoMem);
		else if (IsEmpty()) SetRetCode(DataEmpty);
		else {
			DataCount--;
			return pMem[top--];
		}
		return -1;
	}

	int TStack::IsValid() {
		if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) return 0;
		return 1;
	}

	void TStack::Print() {
		for (int i = 0; i < DataCount; i++)
			cout << pMem[i] << " ";
		cout << endl;
	}
	
	**Описание методов:**
	- *void TStack::Put(const TData &Val)* - Положить элемент в стек (расширение стека в случае необходимости).
	- *TData TStack::Get()* - Получить верхний элемент стека.
	- *TData TStack::Pop()* - Получить верхний элемент стека с его последующим удалением из стека.
	- *int TStack::IsValid()* - Проверка существования стека.
	- *void TStack::Print()* - Печать стека в консоль.

	
### 4. Тесты **TStack**

**Демонстрационная программа** (поставлялась вместе с заданием)

	void main()
	{
	  TStack st(2);
	  int temp;

	  setlocale(LC_ALL, "Russian");
	  cout << "Testing Stack" << endl;
	  for (int i = 0; i < 10; i++)
	  {
		st.Put(i);
		cout << "Put value " << i << " Code " << st.GetRetCode() << endl;
	  }
	  while (!st.IsEmpty())
	  {
		temp = st.Pop();
		cout << "Pop value " << temp << " Code " << st.GetRetCode() << endl;
	  }
	}
	
**Результат**
![](testStackScreen.png)

Кроме того, методы класса **TStack** были протестированы с помощью фреймворка **Google Test**.

	TEST(TStack, fill_up_to_a_maximum)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(1);
		EXPECT_EQ(A.IsFull(), true);
	}

	TEST(TStack, is_full_test)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		EXPECT_NE(A.IsFull(), true);
	}

	TEST(TStack, is_empty_test_true)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(1);
		A.Pop();
		A.Pop();
		EXPECT_EQ(A.IsEmpty(), true);
	}

	TEST(TStack, is_empty_test_false)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(1);
		A.Pop();
		EXPECT_NE(A.IsEmpty(), true);
	}


	TEST(TStack, memory_is_empty_on_adding)
	{
		TStack A(0);
		A.Put(1);
		EXPECT_EQ(DataNoMem, A.GetRetCode());
	}

	TEST(TStack, memory_is_empty_on_removing)
	{
		TStack A(0);
		A.Get();
		EXPECT_EQ(DataNoMem, A.GetRetCode());
	}

	TEST(TStack, dataok_ret_code)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(1);
		EXPECT_EQ(DataOK, A.GetRetCode());
	}

	TEST(TStack, can_get_value)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(2);
		EXPECT_EQ(2, A.Get());
	}

	TEST(TStack, can_insert_element_in_full_stack)
	{
		const int size = 2;
		TStack A(size);
		A.Put(1);
		A.Put(2);
		A.Put(3);
		EXPECT_EQ(3, A.Get());
	}

	TEST(TStack, can_get_code_return_is_empty)
	{
		const int size = 2;
		TStack A(size);
		A.Get();
		EXPECT_EQ(DataEmpty, A.GetRetCode());
	}

**Результат**
![](teststack.png)

	
### 5. Проверка арифметического выражения

Метод CheckExpression(string str) был написан с использованием ранее написанного класса *TStack*, а также стандартного инклюда для работы со строками *<string>*.

	int CheckExpression(string str) {
		TStack openBraces(10);
		int braceNum = 1, errors = 0;
		cout << "Expression: " << str << endl;
		cout << "Braces print: " << endl;
		for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
			if (*iter == '(') {
				openBraces.Put(braceNum);
				braceNum++;
			}
			else if (*iter == ')') {
				if (openBraces.IsEmpty()) {
					cout << "0 - " << braceNum++ << endl;
					errors++;
				}
				else {
					cout << openBraces.Pop() << " - " << braceNum++ << endl;
				}
			}
		}
		while (!openBraces.IsEmpty()) {
			cout << "0 - " << openBraces.Pop() << endl;
		}
		cout << "Errors: " << errors << endl << endl;
		return errors;
	}
	
**Описание алгоритма**

В рамках данной лабораторной работы контроль ограничен только проверкой правильности расстановки скобок. Было проанализировано соответствие открывающих и закрывающих круглых скобок во введённом арифметическом выражении. Программа печатает таблицу соответствия скобок, в которой указано, для каких скобок отсутствуют парные им, а также общее количество найденных ошибок. Для идентификации скобок использованы их порядковые номера в выражении. 
Прочерки в таблице обозначают отсутствие соответствующей скобки. При отсутствии обнаруженных ошибок программа выдает соответствующее сообщение.

На вход алгоритма поступает строка символов, на выходе выдается таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.

- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

	
### 6. Разбор и вычисление арифметического выражения

**Понятие постфиксной формы записи выражения**
В рамках данного задания был разработан алгоритм и составлена программа для перевода арифметического выражения из инфиксной формы записи в постфиксную. **Инфиксная форма** записи характеризуется наличием знаков операций между операндами. Например,

```
(1+2)/(3+4*6.7)-5.3*4.4
```


При такой форме записи порядок действий определяется расстановкой скобок и приоритетом операций. **Постфиксная форма записи** не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:

```
1 2+ 3 4 6.7*+/ 5.3 4.4* -
```

**Алгоритм перевода выражения в постфиксную форму**

Данный алгоритм основан на использовании стека.
На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой.
Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3


Предполагается, что входная строка содержит синтаксически правильное выражение.

Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:

- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек.
Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают.
После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

**Алгоритм вычисления значения выражения в постфиксной форме**

Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек. При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

**Код**

**Используемые методы**
- *int GetPriority(char c)* - Получение приоритета операции (в соответствии с указанной выше таблицей)
- *string DeleteSpaces(string str)* - Удаление пробелов из строки.
- *string ConvertToPostfixForm(string str)* - Перевод выражения в постфиксную форму записи.
- *double ComputeExpression(string str)* - Вычисление выражения.
- *double Compute(string str)* - Основная функция: удаление пробелов, проверка правильности введенного выражения, перевод его в постфиксную форму записи, вычисление значения.

	int GetPriority(char c) {
		switch (c) {
			case '(':
				return 0;
				break;
			case ')':
				return 1;
				break;
			case '+':
				return 2;
				break;
			case '-':
				return 2;
				break;
			case '*':
				return 3;
				break;
			case '/':
				return 3;
				break;
		}
		return -1;
	}

	string DeleteSpaces(string str) {
		for (string::iterator it = str.begin(); it<str.end(); ++it) {
			if (*it == ' ') str.erase(it);
		}
		return str;
	}

	string ConvertToPostfixForm(string str) { // Конвертирование выражения в постфиксную форму
		TStack operations(10);
		string temp;
		bool apStarted = false;
		for (string::iterator it = str.begin(); it<str.end(); ++it) {
			int priority = GetPriority(*it);
			if (priority == -1) { // операнд
				temp += *it;
				if (!apStarted) apStarted = true;
			}
			else {
				static int oldOp = -1;
				if (apStarted == true) {
					apStarted = false;
					temp += " ";
				}
				if (priority == 1) { // обработка закрывающей скобки
					while (GetPriority(operations.Get()) != 0) {
						temp += operations.Pop();
					}
					operations.Pop();
				}
				else if (priority == 0 || priority > oldOp || operations.IsEmpty()) {
					oldOp = *it;
					operations.Put(oldOp);
				}
				else {
					while (GetPriority(operations.Get()) >= priority) {
						temp += operations.Pop();
					}
					temp += " ";
					oldOp = *it;
					operations.Put(oldOp);
				}
			}
		}
		while (!operations.IsEmpty()) {
			temp += operations.Pop();
		}
		return temp;
	}

	double ComputeExpression(string str) {
		string temp;
		stack<double> operands;
		for (string::iterator it = str.begin(); it<str.end(); ++it) {
			int priority = GetPriority(*it);
			if (priority == -1 && *it != ' ') { // операнд
				temp += *it;
			}
			else {
				if (temp.length()) {
					operands.push(atof(temp.c_str()));
					temp.clear();
				}
				if (priority >= 2) { // операция
					double val[2], result = 0;
					for (int i = 0; i < 2; i++) {
						val[i] = operands.top();
						operands.pop();
					}
					switch (*it) {
						case '+':
							result  = val[0] + val[1];
							break;
						case '-':
							result = val[1] - val[0];
							break;
						case '*':
							result = val[0] * val[1];
							break;
						case '/':
							result = val[1] / val[0];
							break;
					}
					operands.push(result);
				}
			}
		}
		return operands.top();
	}

	double Compute(string str) {
		str = DeleteSpaces(str);
		if (CheckExpression(str) == 0) {
			return ComputeExpression(ConvertToPostfixForm(str));
		}
		return 0;
	}


### 7. Тестирование методов работы с арифметическим выражением

Написанные для вычисления значения арифметического выражения методы были протестированы с помощью фреймворка **Google Test**.

	TEST(expressionsTest, translation_of_simple_expression)
	{
		EXPECT_EQ(ConvertToPostfixForm("2*(8-5)+4"), "2 8 5-* 4+");
	}

	TEST(expressionsTest, translating_a_simple_expression_of_double)
	{
		EXPECT_EQ(ConvertToPostfixForm("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
	}


	TEST(expressionsTest, translating_many_digit_values)
	{
		EXPECT_EQ(ConvertToPostfixForm("100+20000/3500*(1000+21000000)"), "100 20000 3500/ 1000 21000000+*+");
	}

	TEST(expressionsTest, translation_bid_double_expressions)
	{
		EXPECT_EQ(ConvertToPostfixForm("(0.0000032+0.0000068)*100"), "0.0000032 0.0000068+ 100*");
	}

	TEST(expressionsTest, not_correct_placement_of_brace)
	{
		EXPECT_NE(CheckExpression("2*(8-5)+4)"), 0);
	}

	TEST(expressionsTest, correct_placement_of_brace)
	{
		EXPECT_EQ(CheckExpression("2*(8-5)+4"), 0);
	}

	TEST(expressionsTest, compute_of_simple_expression)
	{
		float res = 2 * (8 - 5) + 4;
		EXPECT_FLOAT_EQ(Compute("2*(8-5)+4"), res);
	}

	TEST(expressionsTest, compute_a_simple_expression_of_the_real)
	{
		float res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
		EXPECT_FLOAT_EQ(Compute("(1+2)/(3+4*6.7)-5.3*4.4"), res);
	}

	TEST(expressionsTest, compute_many_digit_number)
	{
		float res = 100 + 20000.f / 3500 * (1000 + 21000000);
		EXPECT_FLOAT_EQ(Compute("100+20000/3500*(1000+21000000)"), res);
	}

	TEST(expressionsTest, compute_expression_with_real_lot_of_decimal_places)
	{
		float res = (0.0000032 + 0.0000068) * 100;
		EXPECT_FLOAT_EQ(Compute("(0.0000032+0.0000068)*100"), res);
	}

	TEST(expressionsTest, correct_placement_of_parentheses)
	{
		ASSERT_NO_THROW(Compute("(1+2)/(3+4*6.7)-5.3*4.4"));
	}

![](exprtest.png)


## Выводы

В ходе выполнения данной работы были получены навыки работы с классом Stack. 
Были реализованы два собственных класса - TSimpleStack и TStack, с их помощью была решена задача по вычислению значения арифметического выражения путем перевода его в так называемую постфиксную форму записи.
В ходе написания тестовых программ, а также проверки написанного кода при помощи фреймворка Google Test было установлено, что программа верно решает поставленную задачу.