#include "gtest.h"

#include "expressions.h"

TEST(expressionsTest, translation_of_simple_expression)
{
	EXPECT_EQ(ConvertToPostfixForm("2*(8-5)+4"), "2 8 5-* 4+");
}

TEST(expressionsTest, translating_a_simple_expression_of_double)
{
	EXPECT_EQ(ConvertToPostfixForm("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}


TEST(expressionsTest, translating_many_digit_values)
{
	EXPECT_EQ(ConvertToPostfixForm("100+20000/3500*(1000+21000000)"), "100 20000 3500/ 1000 21000000+*+");
}

TEST(expressionsTest, translation_bid_double_expressions)
{
	EXPECT_EQ(ConvertToPostfixForm("(0.0000032+0.0000068)*100"), "0.0000032 0.0000068+ 100*");
}

TEST(expressionsTest, not_correct_placement_of_brace)
{
	EXPECT_NE(CheckExpression("2*(8-5)+4)"), 0);
}

TEST(expressionsTest, correct_placement_of_brace)
{
	EXPECT_EQ(CheckExpression("2*(8-5)+4"), 0);
}

TEST(expressionsTest, compute_of_simple_expression)
{
	float res = 2 * (8 - 5) + 4;
	EXPECT_FLOAT_EQ(Compute("2*(8-5)+4"), res);
}

TEST(expressionsTest, compute_a_simple_expression_of_the_real)
{
	float res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_FLOAT_EQ(Compute("(1+2)/(3+4*6.7)-5.3*4.4"), res);
}

TEST(expressionsTest, compute_many_digit_number)
{
	float res = 100 + 20000.f / 3500 * (1000 + 21000000);
	EXPECT_FLOAT_EQ(Compute("100+20000/3500*(1000+21000000)"), res);
}

TEST(expressionsTest, compute_expression_with_real_lot_of_decimal_places)
{
	float res = (0.0000032 + 0.0000068) * 100;
	EXPECT_FLOAT_EQ(Compute("(0.0000032+0.0000068)*100"), res);
}

TEST(expressionsTest, correct_placement_of_parentheses)
{
	ASSERT_NO_THROW(Compute("(1+2)/(3+4*6.7)-5.3*4.4"));
}
